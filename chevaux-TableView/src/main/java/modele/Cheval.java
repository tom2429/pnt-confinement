package modele;

public class Cheval {

    private static Integer ID = 0;
    private int id;
    private String nom;
    private int age;

    private Cheval(int id, String nom, int age) {
        this.id = id;
        this.nom = nom;
        this.age = age;
    }

    public static Cheval creer(String nom, int age) {
        return new Cheval(ID++, nom, age);
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

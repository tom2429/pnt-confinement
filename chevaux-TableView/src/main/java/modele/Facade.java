package modele;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Facade {

    private Map<Integer, Cheval> chevalEnregistres;

    private Facade() {
        this.chevalEnregistres = new HashMap<>();
    }

    public static Facade creer() {
        return new Facade();
    }

    public int ajouterCheval(String nom, int age) {
        Cheval nouveauCheval = Cheval.creer(nom, age);
        chevalEnregistres.put(nouveauCheval.getId(), nouveauCheval);
        return nouveauCheval.getId();
    }

    public Cheval getChevalById(int id) throws ChevalNotFoundException {
        Cheval chevalChercher = chevalEnregistres.get(id);
        if (Objects.isNull(chevalChercher)) {
            throw new ChevalNotFoundException();
        }
        return chevalChercher;
    }

    public Collection<Cheval> getChevaux() {
        return this.chevalEnregistres.values();
    }
}

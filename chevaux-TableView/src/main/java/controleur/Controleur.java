package controleur;

import javafx.stage.Stage;
import modele.Cheval;
import modele.ChevalNotFoundException;
import modele.Facade;
import view.VueCheval;
import view.VueChevaux;
import view.VueCreationCheval;

import java.util.Collection;

public class Controleur {

    private Stage stage;
    private Cheval chevalSelect;
    private Facade facade;

    private VueChevaux vueChevaux;
    private VueCheval vueCheval;
    private VueCreationCheval vueCreationCheval;

    public Controleur(Stage stage, Facade facade) {
        this.stage = stage;
        this.facade = facade;
        this.vueChevaux = VueChevaux.creer(this.stage);
        this.vueChevaux.setControleur(this);
        this.vueChevaux.initialisationData();
        this.vueCheval = VueCheval.creer(this.stage);
        this.vueCreationCheval = VueCreationCheval.creer(this.stage);
        this.vueCheval.setControleur(this);
        this.vueCreationCheval.setControleur(this);

    }

    public void run() {
        this.vueChevaux.show();
    }

    public Collection<Cheval> getChevaux() {
        return this.facade.getChevaux();
    }

    public void creerCheval(String nom, int age) throws ChevalNotFoundException {
        int id = this.facade.ajouterCheval(nom, age);
        this.vueChevaux.ajouterCheval(this.facade.getChevalById(id));
        this.vueChevaux.show();
    }

    public void creerCheval() {
        this.vueCreationCheval.show();
    }


    public void afficherCheval(int id) throws ChevalNotFoundException {
        this.chevalSelect = this.facade.getChevalById(id);
        this.vueCheval.initData();
        this.vueCheval.show();
    }

    public void back() {
        this.vueChevaux.show();
    }

    public Cheval getChevalSelect() {
        return this.chevalSelect;
    }
}

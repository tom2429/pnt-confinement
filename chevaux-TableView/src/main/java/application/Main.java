package application;

import controleur.Controleur;
import javafx.application.Application;
import javafx.stage.Stage;
import modele.Facade;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Facade facade = Facade.creer();
        facade.ajouterCheval("Jolly Jumper", 55);
        facade.ajouterCheval("Silas", 35);
        facade.ajouterCheval("Tornado", 35);


        Controleur monControleur = new Controleur(primaryStage, facade);
        monControleur.run();
    }
}

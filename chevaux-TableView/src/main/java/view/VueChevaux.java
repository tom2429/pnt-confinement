package view;

import controleur.Controleur;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import modele.Cheval;
import modele.ChevalNotFoundException;

import java.util.Collection;

public class VueChevaux implements Vue, ControleurAware {

    private Stage stage;
    private Scene scene;
    private TableView<Cheval> tableView;
    private Controleur controleur;

    private VueChevaux(Stage stage) {
        this.stage = stage;
        BorderPane borderPane = new BorderPane();
        this.tableView = new TableView<Cheval>();
        borderPane.setCenter(this.tableView);

        Label titre = new Label("Liste des chevaux");
        titre.setFont(Font.font(30));
        borderPane.setTop(titre);
        Button ajoutCheval = new Button("Ajouter un cheval");
        ajoutCheval.setOnAction(e -> this.controleur.creerCheval());
        BorderPane.setAlignment(ajoutCheval, Pos.CENTER);
        borderPane.setBottom(ajoutCheval);
        this.scene = new Scene(borderPane);
    }

    public static VueChevaux creer(Stage stage) {
        return new VueChevaux(stage);
    }

    public void initialisationData() {
        Collection<Cheval> chevaux = this.controleur.getChevaux();
        this.tableView.getItems().addAll(chevaux);
        this.preparerTableView();
    }

    private void preparerTableView() {
        this.tableView.setEditable(false);

        this.tableView.setRowFactory(tv -> {
            TableRow<Cheval> row = new TableRow<Cheval>();
            row.setOnMouseClicked(mouseEvent -> {
                doForClick(mouseEvent, row);
            });
            return row;
        });

        TableColumn<Cheval, Number> idCol = new TableColumn<Cheval, Number>("id Cheval");
        TableColumn<Cheval, String> nomCheval = new TableColumn<Cheval, String>("nom Cheval");

        idCol.setCellValueFactory(new PropertyValueFactory<Cheval, Number>("id"));
        nomCheval.setCellValueFactory(new PropertyValueFactory<Cheval, String>("nom"));

        this.tableView.getColumns().addAll(idCol, nomCheval);
    }

    private void doForClick(MouseEvent mouseEvent, TableRow<Cheval> row) {
        if (mouseEvent.getClickCount() == 2 && mouseEvent.getButton() == MouseButton.PRIMARY && !row.isEmpty()) {
            try {
                this.controleur.afficherCheval(row.getItem().getId());
            } catch (ChevalNotFoundException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Le cheval sélectionné n'existe pas", ButtonType.OK);
                alert.showAndWait();
            }
        }
    }

    @Override
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    @Override
    public void show() {
        this.stage.setScene(this.scene);
        this.stage.show();
    }

    public void ajouterCheval(Cheval cheval) {
        this.tableView.getItems().add(cheval);
    }
}

package view;

import controleur.Controleur;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import modele.ChevalNotFoundException;

public class VueCreationCheval implements Vue, ControleurAware {

    private Controleur controleur;
    private Stage stage;
    private Scene scene;

    private TextField nomCheval;
    private TextField ageCheval;

    public VueCreationCheval(Stage stage) {
        this.stage = stage;
        Label titre = new Label("Nouveau Cheval");
        titre.setFont(Font.font(30));

        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10d);


        Label nomChevalLb = new Label("nom Cheval");
        Label ageChevalLb = new Label("age Cheval");

        this.nomCheval = new TextField();

        this.ageCheval = new TextField();

        Button creer = new Button("Créer");
        creer.setOnAction(e -> this.creerCheval(e));
        Button back = new Button("Back");
        back.setOnAction(e -> this.controleur.back());

        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER);
        hBox.setSpacing(15);
        hBox.getChildren().addAll(creer, back);

        vBox.getChildren().addAll(titre, nomChevalLb, this.nomCheval, ageChevalLb, this.ageCheval, hBox);
        this.scene = new Scene(vBox);
    }

    public static VueCreationCheval creer(Stage stage) {
        return new VueCreationCheval(stage);
    }

    private void creerCheval(ActionEvent e) {
        if (this.nomCheval.getText().length() == 0 || this.ageCheval.getText().length() == 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Les champs sont obligatoires !!!", ButtonType.OK);
            alert.showAndWait();
        } else {
            try {
                this.controleur.creerCheval(nomCheval.getText(), Integer.parseInt(ageCheval.getText()));
            } catch (ChevalNotFoundException ex) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Problème lors de la création du cheval", ButtonType.OK);
                alert.showAndWait();
            }
        }
    }

    @Override
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    @Override
    public void show() {
        this.stage.setScene(this.scene);
        this.stage.show();
    }
}

package view;

import controleur.Controleur;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import modele.Cheval;

public class VueCheval implements Vue, ControleurAware {

    private Controleur controleur;
    private Stage stage;
    private Scene scene;

    private TextField idCheval;
    private TextField nomCheval;
    private TextField ageCheval;

    public VueCheval(Stage stage) {
        this.stage = stage;
        Label titre = new Label("Le Cheval");
        titre.setFont(Font.font(30));

        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10d);

        Label idChevalLb = new Label("ID Cheval");
        Label nomChevalLb = new Label("nom Cheval");
        Label ageChevalLb = new Label("age Cheval");

        this.idCheval = new TextField();
        this.idCheval.setDisable(true);
        this.nomCheval = new TextField();
        this.nomCheval.setDisable(true);
        this.ageCheval = new TextField();
        this.ageCheval.setDisable(true);

        Button back = new Button("Back");
        back.setOnAction(e -> this.controleur.back());

        vBox.getChildren().addAll(titre, idChevalLb, idCheval, nomChevalLb, nomCheval, ageChevalLb, ageCheval, back);
        this.scene = new Scene(vBox);
    }

    public static VueCheval creer(Stage stage) {
        return new VueCheval(stage);
    }

    public void initData() {
        Cheval chevalSelect = this.controleur.getChevalSelect();
        this.idCheval.setText(String.valueOf(chevalSelect.getId()));
        this.nomCheval.setText(chevalSelect.getNom());
        this.ageCheval.setText(String.valueOf(chevalSelect.getAge()));

    }

    @Override
    public void setControleur(Controleur controleur) {
        this.controleur = controleur;
    }

    @Override
    public void show() {
        this.stage.setScene(this.scene);
        this.stage.show();
    }
}

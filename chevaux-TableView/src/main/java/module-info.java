module chevaux.TableView {

    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    exports application;
    opens view to javafx.fxml;
    opens modele to javafx.base;
}